# OpenStack ArgoCD

## Installation

### Base / Pre-Requisites

For now we have a pre-requisite for a vault instance inside the cluster.
```bash
helm -n base install base ./
```

Follow the instructions under base/vault-howto to unseal and enable the kv
backend, and create a token and secret to be used by argocd-vaultplugin.

This is required for the argocd-reposerver to inject the secret data.

### ArgoCD

#### Client

```bash
wget -O ~/bin/argo https://github.com/argoproj/argo-cd/releases/download/v2.1.6/argocd-linux-amd64
chmod 755 ~/bin/argocd
```

#### Server

```bash
kubectl create namespace argocd
helm repo add argo https://argoproj.github.io/argo-helm
helm repo update
helm -n argocd install argocd argo/argo-cd --values argocd/values.yaml
```

Install the ApplicationSet CRD:
```bash
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj-labs/applicationset/v0.2.0/manifests/install.yaml
```

UI is available via a serviceType: LoadBalancer, user 'admin' and password is in the secret below:
```bash
kubectl -n argocd get service argocd-server -o jsonpath="{.status.loadBalancer.ingress[0].ip}"
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
```

### Clusters

Registering a new cluster:
```bash
argocd cluster add --kubeconfig config --name metar-child-001 default -y
```

We need some additional labels, these must be set in the corresponding cluster secret:
```bash
kubectl -n argocd patch secret cluster-188.185.127.194-1337456898 --patch '{ "metadata": { "labels": { "cloud": "cern" }}}'
```

### Monitoring

In addition to centralized data with Thanos, Prometheus and cost dashboards are exposed via load balancers in each region.

* Prometheus: http://$PROMETHEUSIP:9090
```bash
PROMETHEUSIP=$(kubectl --context KUBECONTEXT -n addons get service prom-prometheus -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
```
* Grafana: http://$GRAFANAIP
```bash
GRAFANAIP=$(kubectl --context KUBECONTEXT -n addons get service prom-grafana -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
```
* Kubecost: http://$KUBECOSTIP:9090
```bash
KUBECOSTIP=$(kubectl --context KUBECONTEXT -n addons get service aks-gitlab-ci-northeur-20211115-costanalyzer-cost-analyzer -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
```

### TODO

* One of the prometheus CRDs needs manual creation (with apply it complains
  about 'too many bytes':
```bash
kubectl create -f cluster-addons/monit/templates/crd-prometheuses.yaml
```

* Kubecost service name has a cluster name prefix - the `fullnameOverride` is
  applying to the pod but not the service
